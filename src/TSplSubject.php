<?php

namespace jf\event;

use SplObserver;
use SplSubject;

/**
 * Trait que implementa la interfaz `SplSubject`.
 *
 * @mixin SplSubject
 */
trait TSplSubject
{
    use TObservers;

    /**
     * @see SplSubject::attach()
     */
    public function attach(SplObserver $observer) : void
    {
        if ($observer instanceof IObserver)
        {
            foreach ($observer->observedEvents() as $_event)
            {
                if ($_event && is_string($_event))
                {
                    $this->addObserver($_event, $observer);
                }
            }
        }
        else
        {
            $this->addObserver('', $observer);
        }
    }

    /**
     * @see SplSubject::detach()
     */
    public function detach(SplObserver $observer) : void
    {
        $this->removeObserver($observer);
    }

    /**
     * Notifica a TODOS los observadores.
     * Al no provenir de un evento el sujeto que se pasa es el propio manager.
     *
     * @see SplSubject::notify()
     */
    public function notify() : void
    {
        foreach ($this->getObservers() as $_observer)
        {
            $_observer->update($this);
        }
    }
}