<?php

namespace jf\event;

use Psr\EventDispatcher\EventDispatcherInterface;
use SplObserver;
use SplSubject;

/**
 * Manejador simple de eventos.
 */
class Manager implements IObserver, ISubject
{
    use TDispatcher
    {
        TDispatcher::dispatch as private _dispatch;
        TDispatcher::notify as private _notify;
    }

    /**
     * Configuración de los eventos gestionados por la clase de manera dinámica.
     *
     * @var array<class-string,class-string[]>|null
     */
    protected ?array $_events = NULL;

    /**
     * Constructor de la clase
     *
     * @param array|null $events Configuración de los eventos gestionados por la clase de manera dinámica.
     */
    public function __construct(?array $events = NULL)
    {
        $this->_events = $events;
    }

    /**
     * @see EventDispatcherInterface::dispatch()
     */
    public function dispatch(object $event) : static
    {
        return $this->loadEvent($event::class)->_dispatch($event);
    }

    /**
     * Carga los eventos que se han agregado como nombres de clase y agrega los
     * observadores del evento especificado.
     *
     * El uso de este método permite optimizar los recursos en la gestión de eventos
     * ya que en lugar de generar instancias de observadores que no procesarán ningún
     * evento se van creando a medida que ocurren los eventos.
     *
     * @param string|null $name Nombre del evento a procesar.
     *
     * @return static
     */
    protected function loadEvent(?string $name = NULL) : static
    {
        if ($name !== NULL)
        {
            $_observers = $this->_events[ $name ] ?? NULL;
            if ($_observers)
            {
                foreach ($_observers as $_observer)
                {
                    if (is_string($_observer) && class_exists($_observer))
                    {
                        $_observer = new $_observer();
                    }
                    if ($_observer instanceof IObserver)
                    {
                        if (in_array($name, $_observer->observedEvents()))
                        {
                            $this->attach($_observer);
                        }
                    }
                    else if ($_observer instanceof SplObserver)
                    {
                        $this->addObserver($name, $_observer);
                    }
                }
            }
            unset($this->_events[ $name ]);
        }

        return $this;
    }

    /**
     * @see SplSubject::notify()
     */
    public function notify() : void
    {
        $this->loadEvent('')->_notify();
    }

    /**
     * @inheritdoc
     */
    public function observedEvents() : array
    {
        return array_unique([ ...array_keys($this->_observers), ...array_keys($this->loadEvent()->_events) ]);
    }

    /**
     * @inheritdoc
     */
    public function update(SplSubject $subject) : void
    {
        $this->dispatch($subject);
    }
}