<?php

namespace jf\event;

use DateTimeImmutable;
use Psr\EventDispatcher\StoppableEventInterface;
use SplSubject;

/**
 * Interfaz para los eventos que se emiten.
 */
interface IEvent extends StoppableEventInterface
{
    /**
     * Identificador del evento.
     *
     * @return int|string|NULL
     */
    public function id() : int|string|null;

    /**
     * Momento en el que ocurre el evento.
     *
     * @return DateTimeImmutable
     */
    public function occurredOn() : DateTimeImmutable;

    /**
     * Devuelve el sujeto que ha emitido el evento.
     *
     * @return SplSubject|NULL
     */
    public function subject() : ?SplSubject;
}