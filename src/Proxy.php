<?php

namespace jf\event;

use SplObserver;

/**
 * Evento genérico usado como proxy al objeto almacenado en `subject` en aquellos caso
 * en los que se quiera pasar al `update` de los observadores el evento directamente.
 *
 * Implementa la interfaz `SplSubject` como proxy de la instancia almacenada en la
 * propiedad `$subject` solamente para poder pasar el evento al método `update` que
 * requiere como parámetro una clase que implemente dicha interfaz, pero llamar a
 * cualquiera de los métodos de esa interfaz será como llamar a los métodos de la
 * instancia almacenada en `$subject`.
 */
class Proxy extends Event
{
    /**
     * Datos enviados en el evento.
     *
     * @var mixed|null
     */
    public mixed $data = NULL;

    /**
     * @inheritdoc
     */
    public function attach(SplObserver $observer) : void
    {
        $this->subject()->attach($observer);
    }

    /**
     * @inheritdoc
     */
    public function detach(SplObserver $observer) : void
    {
        $this->subject()->detach($observer);
    }

    /**
     * Crea una nueva instancia del evento asignando los valores
     * de la configuración que son propiedades de la instancia y
     * guardando el resto en la propiedad `data` del evento.
     *
     * @param array $config Configuración a aplicar a la instancia.
     *
     * @return static
     */
    public static function new(array $config = []) : static
    {
        $_event = parent::new($config);
        foreach ($config as $_property => $_value)
        {
            if (property_exists($_event, $_property))
            {
                unset($config[ $_property ]);
            }
        }
        if ($config && $_event->data === NULL)
        {
            $_event->data = $config;
        }

        return $_event;
    }

    /**
     * @inheritdoc
     */
    public function notify() : void
    {
        $this->subject()->notify();
    }
}