<?php

namespace jf\event;

use Psr\EventDispatcher\EventDispatcherInterface;
use Psr\EventDispatcher\ListenerProviderInterface;
use SplSubject;

/**
 * Trait que facilita la implementación de las interfaces `SplSubject`,
 * `Psr\EventDispatcher\EventDispatcherInterface` y `Psr\EventDispatcher\ListenerProviderInterface`.
 *
 * @mixin EventDispatcherInterface
 * @mixin ListenerProviderInterface
 * @mixin SplSubject
 */
trait TDispatcher
{
    use TEventDispatcher;
    use TSplSubject;
}