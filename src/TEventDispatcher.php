<?php

namespace jf\event;

use Psr\EventDispatcher\EventDispatcherInterface;
use Psr\EventDispatcher\ListenerProviderInterface;
use Psr\EventDispatcher\StoppableEventInterface;
use SplObserver;
use SplSubject;

/**
 * Trait que implementa las interfaces `Psr\EventDispatcher\EventDispatcherInterface`
 * y `Psr\EventDispatcher\ListenerProviderInterface`.
 *
 * @mixin EventDispatcherInterface
 * @mixin ListenerProviderInterface
 */
trait TEventDispatcher
{
    use TObservers;

    /**
     * @see EventDispatcherInterface::dispatch()
     */
    public function dispatch(object $event) : static
    {
        if ($event instanceof SplSubject)
        {
            $_subject = $event;
        }
        else if ($event instanceof IEvent)
        {
            $_subject = $event->subject();
        }
        else
        {
            $_subject = NULL;
        }
        if ($_subject)
        {
            $_stoppable = $event instanceof StoppableEventInterface;
            /** @var SplObserver $_observer */
            foreach ($this->getListenersForEvent($event) as $_observer)
            {
                $_observer->update($_subject);
                if ($_stoppable && $event->isPropagationStopped())
                {
                    break;
                }
            }
        }

        return $this;
    }

    /**
     * @see ListenerProviderInterface::getListenersForEvent()
     */
    public function getListenersForEvent(object $event) : array
    {
        return $this->_observers[ $event::class ] ?? [];
    }
}