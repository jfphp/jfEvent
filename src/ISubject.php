<?php

namespace jf\event;

use Psr\EventDispatcher\EventDispatcherInterface;
use Psr\EventDispatcher\ListenerProviderInterface;
use SplSubject;

/**
 * Interfaz para los emisores de eventos.
 *
 * Reúne en una sola interfaz las diferentes interfaces que se han definido
 * en PHP para la gestión de eventos (SPL y PSR).
 */
interface ISubject extends EventDispatcherInterface, ListenerProviderInterface, SplSubject
{
}