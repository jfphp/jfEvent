<?php

namespace jf\event;

use SplObserver;

/**
 * Interfaz para los observadores de los eventos.
 */
interface IObserver extends SplObserver
{
    /**
     * Listado de los eventos observados.
     *
     * @return string[]
     */
    public function observedEvents() : array;
}