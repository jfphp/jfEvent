<?php

namespace jf\event;

use DateTimeImmutable;
use SplSubject;

/**
 * Trait para implementar la interfaz `IEvent`.
 *
 * @mixin IEvent
 */
trait TEvent
{
    /**
     * Contador de eventos.
     *
     * @var int
     */
    private static int $_eventCounter = 1;

    /**
     * Objeto que a pasar al observador.
     * Se puede usar en aquellos casos en los que el evento no sea de tipo `SplSubject`.
     *
     * @var SplSubject|NULL
     */
    protected ?SplSubject $subject = NULL;

    /**
     * @see IEvent::id()
     */
    public function id() : int|string|null
    {
        return sprintf('%s(%s)', static::class, static::$_eventCounter++);
    }

    /**
     * @see IEvent::occurredOn()
     */
    public function occurredOn() : DateTimeImmutable
    {
        return new DateTimeImmutable();
    }

    /**
     * @see IEvent::subject()
     */
    public function subject() : ?SplSubject
    {
        return $this->subject;
    }
}