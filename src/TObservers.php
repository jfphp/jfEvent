<?php

namespace jf\event;

use SplObserver;

/**
 * Gestiona la lista de observadores de los eventos.
 */
trait TObservers
{
    /**
     * Listado de observadores de los eventos.
     *
     * @var array<class-string, SplObserver[]>
     */
    private array $_observers = [];

    /**
     * Agrega un observador a la lista.
     *
     * @param string      $event    Nombre del evento que escuchará el observador.
     * @param SplObserver $observer Observador que será agregado.
     *
     * @return static
     */
    public function addObserver(string $event, SplObserver $observer) : static
    {
        $this->_observers[ $event ][ $this->getObserverId($observer) ] = $observer;

        return $this;
    }

    /**
     * Elimina la observación todos los eventos.
     *
     * @return static
     */
    public function clear() : static
    {
        $this->_observers = [];

        return $this;
    }

    /**
     * Devuelve el listado de nombres de eventos observados.
     *
     * @return string[]
     */
    public function getEventNames() : array
    {
        return array_keys($this->_observers);
    }

    /**
     * Devuelve el identificador único del observador.
     *
     * @param SplObserver $observer Observador del que se quiere obtener su identificador.
     *
     * @return int|string
     */
    public function getObserverId(SplObserver $observer) : int|string
    {
        return $observer::class . '-' . spl_object_id($observer);
    }

    /**
     * Devuelve todos los observadores registrados para todos los eventos.
     *
     * @return SplObserver[]
     */
    public function getObservers() : array
    {
        $_observers = [];
        foreach ($this->_observers as $_items)
        {
            foreach ($_items as $_item)
            {
                $_observers[ $this->getObserverId($_item) ] = $_item;
            }
        }

        return array_values($_observers);
    }

    /**
     * Elimina la observación de un evento.
     *
     * @param string $event Nombre del evento que será eliminado.
     *
     * @return static
     */
    public function removeEvent(string $event) : static
    {
        unset($this->_observers[ $event ]);

        return $this;
    }

    /**
     * Elimina un observador de la lista y devuelve los nombres de los eventos
     * de los cuales fue eliminado.
     *
     * @param SplObserver $observer Observador que será eliminado.
     *
     * @return string[]
     */
    public function removeObserver(SplObserver $observer) : array
    {
        $_events = [];
        $_id     = $this->getObserverId($observer);
        foreach ($this->_observers as $_event => $_observers)
        {
            if (array_key_exists($_id, $_observers))
            {
                unset($this->_observers[ $_event ][ $_id ]);
                $_events[] = $_event;
            }
        }

        return $_events;
    }
}