<?php

namespace jf\event;

use SplObserver;
use SplSubject;

/**
 * Clase simple para eventos genéricos.
 */
class Event implements IEvent, SplSubject
{
    use TEvent;
    use TStoppable;

    /**
     * Nombre del evento.
     *
     * Permite diferenciar distintos eventos que usen esta
     * clase sin necesidad de extenderla.
     *
     * @var string
     */
    public string $type = '';

    /**
     * @inheritdoc
     */
    public function attach(SplObserver $observer) : void
    {
    }

    /**
     * @inheritdoc
     */
    public function detach(SplObserver $observer) : void
    {
    }

    /**
     * @inheritdoc
     */
    public function notify() : void
    {
    }

    /**
     * Crea una nueva instancia del evento asignando los valores
     * de la configuración que son propiedades de la instancia y
     * guardando el resto en la propiedad `data` del evento.
     *
     * @param array $config Configuración a aplicar a la instancia.
     *
     * @return static
     */
    public static function new(array $config = []) : static
    {
        $_event = new static();
        foreach ($config as $_property => $_value)
        {
            if (property_exists($_event, $_property))
            {
                $_event->$_property = $_value;
                unset($config[ $_property ]);
            }
        }
        if (!$_event->type)
        {
            $_pos         = strrpos(static::class, '\\');
            $_event->type = $_pos === FALSE
                ? static::class
                : substr(static::class, $_pos + 1);
        }

        return $_event;
    }
}