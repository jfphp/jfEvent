<?php

namespace jf\event;

use Psr\EventDispatcher\StoppableEventInterface;

/**
 * Trait que implementa la interfaz `Psr\EventDispatcher\StoppableEventInterface`.
 *
 * @mixin StoppableEventInterface
 */
trait TStoppable
{
    private bool $_stopped = FALSE;

    /**
     * @see StoppableEventInterface::isPropagationStopped()
     */
    public function isPropagationStopped() : bool
    {
        return $this->_stopped;
    }

    /**
     * Marca el evento para detener su propagación.
     *
     * @return static
     */
    public function stopPropagation() : static
    {
        $this->_stopped = TRUE;

        return $this;
    }
}